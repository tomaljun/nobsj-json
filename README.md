# NoBSJ JSON library
## Introduction
NoBSJ JSON is a small Java library that can be used to
- Parse String/InputStream/File to JSON
- Serialize JSON to String
- Extract values using a path syntax
- Walk JSON

## Serialize JSON to string
```java
Map<String, Object> map = new HashMap<>();
map.put("property 1", "value 1");
map.put("property 2", "value 2");
System.out.println(Json.toString(map));
List<Object> list = new ArrayList<>();
list.add("item 1");
list.add("item 2");
System.out.println(Json.toString(list));
```

## Parse string to JSON
```java
String json1 = "{\"property 1\":\"value 1\",\"property 2\":\"value 2\"}";
Map<String, Object> map = Json.toMap(json1);
String json2 = "[\"item 1\",\"item 2\"]";
List<Object> list = Json.toList(json2);
```

## Extract value using a path string
```java
Map<String, Object> map = Json.toMap("{\"a\":\"hello\",\"b\":12345,\"c\":{\"d\":true,\"e\":1.01},\"f\":[1,2,3]}");
JsonPath.asString(map, "/a"); // "hello"
JsonPath.asLong(map, "/b"); // 12345
JsonPath.asBoolean(map, "/c/d"); // true
JsonPath.asDouble(map, "/c/e"); // 1.01
JsonPath.asArray(map, "/c"); // {"d":true,"e":1.01}
JsonPath.asObject(map, "/f"); // [1,2,3]
```

## Walk JSON
```java
JsonNode node = JsonNode.wrap(Json.toMap("{\"item 1\":\"value 1\"}"));
node.asObject().get("item 1").asString(); // "value 1"
```
