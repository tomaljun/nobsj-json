package nobsj.json;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.Test;

import mindeggs.nobsj.json.JsonParser;
import mindeggs.nobsj.json.JsonPath;

public class JsonPathTest {

   @Test
   public void testJsonPath() throws Exception {
      JsonParser parser = new JsonParser(new String(Files.readAllBytes(Paths.get(getClass().getResource("/testobject.json").toURI())), StandardCharsets.UTF_8));
      
      Map<String, Object> map = parser.toMap();
      
      assertEquals("e", JsonPath.asString(map, "/c/d"));
      
      try {
         JsonPath.asString(map, "/a/d");
         fail();
      } catch (Exception e) {
         assertEquals("a is not an object", e.getMessage());
      }
      
      assertEquals("j", JsonPath.asString(map, "/h/[1]"));
      assertEquals(new Long(1), JsonPath.asLong(map, "/c/f"));
      assertEquals(true, JsonPath.asBoolean(map, "/c/g"));
      assertEquals(new Double(1.01), JsonPath.asDouble(map, "/l"));
   }
}
