package mindeggs.nobsj.json.util;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

/**
 * Use this class to avoid using String.split() when just splitting on a single
 * character.
 */
public class SplitString {

   private String source;
   private char character;
   private int index;

   /**
    * Constructor create SplitString using specified source and split character
    * 
    * @param source
    *           Source string to split
    * @param character
    *           Split character
    */
   public SplitString(String source, char character) {
      this.source = source;
      this.character = character;
      this.index = 0;
   }

   /**
    * Get next string
    * 
    * @return Next string
    */
   public String next() {
      int length = source.length();
      while (index < length && source.charAt(index) == character) {
         index++;
      }
      int start = index;
      while (index < length && source.charAt(index) != character) {
         index++;
      }
      if (index > start) {
         return source.substring(start, index);
      }
      return null;
   }

}
