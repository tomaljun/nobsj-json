package mindeggs.nobsj.json;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Parse data in JSON format from file or text string
 */
public final class JsonParser {

   private int index;
   private int row;
   private int column;
   private String input;

   /**
    * Create new JsonParser to parse a file
    * 
    * @param file
    *           File containing JSON
    * @throws Exception
    *            If an error occurred
    */
   public JsonParser(File file) throws Exception {
      this(file.toPath());
   }

   /**
    * Create new JsonParse to parse a path (path to file)
    * 
    * @param path
    *           Path pointing to file containing JSON
    * @throws Exception
    *            If an error occurred
    */
   public JsonParser(Path path) throws Exception {
      this(new String(Files.readAllBytes(path)));
   }

   /**
    * Create new JsonParser to parse input stream
    * 
    * @param input
    *           Input stream containing JSON
    * @throws Exception
    *            If an error occurred
    */
   public JsonParser(InputStream input) throws Exception {
      this(new BufferedReader(new InputStreamReader(input)).lines().collect(Collectors.joining()));
   }

   /**
    * Create new JsonParser to parse a string
    * 
    * @param json
    *           String containing json
    */
   public JsonParser(String json) {
      this.input = json;
      index = 0;
      row = 1;
      column = 1;
   }

   /**
    * Parse JSON to Map
    * 
    * @return Map
    * @throws Exception
    *            If an error occurred
    */
   public Map<String, Object> toMap() throws Exception {

      nextNonWhitespace();

      if (input.charAt(index) == '{') {
         return parseObject();
      }

      throw new Exception(formatExpectCharacterMessage('{'));

   }

   /**
    * Parse JSON to List
    * 
    * @return List
    * @throws Exception
    *            If an error occurred
    */
   public List<Object> toList() throws Exception {

      nextNonWhitespace();

      if (input.charAt(index) == '[') {
         return parseArray();
      }

      throw new Exception(formatExpectCharacterMessage('['));

   }

   private void nextNonWhitespace() throws Exception {
      while (Character.isWhitespace(input.charAt(index))) {
         if (input.charAt(index) == '\n') {
            row++;
            column = 1;
         }
         advancePosition();
      }
   }

   private Map<String, Object> parseObject() throws Exception {

      Map<String, Object> object = new HashMap<>();

      while (true) {
         advancePosition();
         nextNonWhitespace();
         if (input.charAt(index) == '}') {
            break;
         }
         if (input.charAt(index) != '"') {
            throw new Exception(formatExpectCharacterMessage('\"'));
         }

         String id = parseString();
         advancePosition();
         nextNonWhitespace();
         if (input.charAt(index) != ':') {
            throw new Exception(formatExpectCharacterMessage(':'));
         }

         advancePosition();
         nextNonWhitespace();
         Object value = null;
         char c = input.charAt(index);
         if (c == '{') {
            value = parseObject();
         } else if (c == '[') {
            value = parseArray();
         } else if (c == '"') {
            value = parseString();

         } else if (Character.isDigit(c)) {
            value = parseNumber();
            index--;
         } else {
            value = parseTrueFalseOrNull();
            index--;
         }

         object.put(id, value);

         advancePosition();
         nextNonWhitespace();

         if (input.charAt(index) != ',') {
            break;
         }

      }

      if (input.charAt(index) != '}') {
         throw new Exception(formatExpectCharacterMessage('}'));
      }

      return object;

   }

   private List<Object> parseArray() throws Exception {

      List<Object> array = new ArrayList<>();

      while (true) {
         advancePosition();
         nextNonWhitespace();
         Object value = null;
         boolean add = true;
         char c = input.charAt(index);
         if (c == '{') {
            value = parseObject();
         } else if (c == '[') {
            value = parseArray();
         } else if (c == '"') {
            value = parseString();
         } else if (Character.isDigit(c)) {
            value = parseNumber();
            index--;
         } else if (c != ']') {
            value = parseTrueFalseOrNull();
            index--;
         } else {
            index--;
            add = false;
         }

         if (add) {
            array.add(value);
         }

         advancePosition();
         nextNonWhitespace();

         if (input.charAt(index) != ',') {
            break;
         }

      }

      if (input.charAt(index) != ']') {
         throw new Exception(formatExpectCharacterMessage(']'));
      }

      return array;

   }

   private String parseString() throws Exception {
      advancePosition();
      int s = index;
      while (input.charAt(index) != '"') {
         if (input.charAt(index) == '\\') {
            advancePosition();
         }
         advancePosition();
      }
      return input.substring(s, index);
   }

   private Object parseNumber() throws Exception {
      int s = index;
      while (Character.isDigit(input.charAt(index))) {
         advancePosition();
      }
      if (input.charAt(index) == '.') {
         advancePosition();
         while (Character.isDigit(input.charAt(index))) {
            advancePosition();
         }
         return Double.parseDouble(input.substring(s, index));
      }

      return Long.parseLong(input.substring(s, index));
   }

   private Boolean parseTrueFalseOrNull() throws Exception {
      int s = index;
      while (Character.isLetter(input.charAt(index))) {
         advancePosition();
      }
      String value = input.substring(s, index);
      if ("true".equals(value) || "false".equals(value)) {
         return new Boolean(value);
      }
      if ("null".equals(value)) {
         return null;
      }
      throw new Exception("Expected true, false or null");
   }

   private void advancePosition() throws Exception {
      index++;
      if (index == input.length()) {
         throw new Exception("End of input");
      }
   }

   private String formatExpectCharacterMessage(char character) {
      StringBuilder builder = new StringBuilder();
      builder.append("Expected '");
      builder.append(character);
      builder.append("' at row: ");
      builder.append(row);
      builder.append(", col: ");
      builder.append(column);
      return builder.toString();
   }

}
