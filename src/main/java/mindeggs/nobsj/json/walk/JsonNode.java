package mindeggs.nobsj.json.walk;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.List;
import java.util.Map;

public class JsonNode {
   
   private List<Object> array;
   private Map<String, Object> object;
   private Object value;
   
   @SuppressWarnings("unchecked")
   public static JsonNode wrap(Object object) {
      if (object instanceof List) {
         return new JsonNode((List<Object>) object);
      } else if (object instanceof Map) {
         return new JsonNode((Map<String, Object>) object);
      }
      return new JsonNode(object);
   }
   
   public JsonObject asObject() {
      return JsonObject.wrap(object);
   }
   
   public JsonArray asArray() {
      return JsonArray.wrap(array);
   }
   
   public String asString() {
      return value.toString();
   }
   
   public Long asInteger() {
      return Long.parseLong(value.toString());
   }
   
   public Boolean asBoolean() {
      return Boolean.parseBoolean(value.toString());
   }
   
   private JsonNode(List<Object> array) {
      this.array = array;
   }
   
   private JsonNode(Map<String, Object> object) {
      this.object = object;
   }
   
   private JsonNode(Object value) {
      this.value = value;
   }

}
