package mindeggs.nobsj.json;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.List;
import java.util.Map;

import mindeggs.nobsj.json.util.SplitString;

/**
 * A class to extract values from a JSON object using an path style pattern
 */
public class JsonPath {

   /**
    * 
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   public static String asString(Object json, String path) throws Exception {
      return locate(json, path).toString();
   }

   /**
    * 
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   public static Long asLong(Object json, String path) throws Exception {
      return (Long) locate(json, path);
   }

   /**
    * 
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   public static Boolean asBoolean(Object json, String path) throws Exception {
      return (Boolean) locate(json, path);
   }

   /**
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   public static Double asDouble(Object json, String path) throws Exception {
      return (Double) locate(json, path);
   }
   
   /**
    * 
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   @SuppressWarnings("unchecked")
   public static List<Object> asArray(Object json, String path) throws Exception {
      return (List<Object>) locate(json, path);
   }
   
   /**
    * 
    * @param json
    * @param path
    * @return
    * @throws Exception
    */
   @SuppressWarnings("unchecked")
   public static Map<String, Object> asObject(Object json, String path) throws Exception {
      return (Map<String, Object>) locate(json, path);
   }

   @SuppressWarnings("unchecked")
   private static Object locate(Object json, String path) throws Exception {
      SplitString splitString = new SplitString(path, '/');
      String previousPart = "root";
      String part = splitString.next();
      while (part != null) {
         if (part.charAt(0) == '[') {
            // Parse index
            int p = part.indexOf(']');
            if (p == -1) {
               throw new Exception("Expected ']' to close index specifier");
            }
            int index = Integer.parseInt(part.substring(1, p));
            if (json instanceof List) {
               json = ((List<Object>) json).get(index);
            } else {
               throw new Exception(previousPart + " is not an array");
            }
         } else {
            // Treat object as a map
            if (json instanceof Map) {
               json = ((Map<String, Object>) json).get(part);
            } else {
               throw new Exception(previousPart + " is not an object");
            }
         }
         previousPart = part;
         part = splitString.next();
      }
      return json;
   }

}
