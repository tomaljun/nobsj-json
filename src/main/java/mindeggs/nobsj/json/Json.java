package mindeggs.nobsj.json;

/*-
 * #%L
 * nobsj-json
 * %%
 * Copyright (C) 2017 Mindeggs
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Use the static methods of this class to:
 * - Convert from JSON to Java Map or List
 * - Convert from Java Map or List to JSON
 */
public final class Json {

   /**
    * Convert a Map instance to JSON
    * 
    * @param map
    *           Map to convert
    * @return JSON
    */
   public static String toString(Map<?, ?> map) {
      StringBuilder builder = new StringBuilder();
      builder.append('{');
      boolean addComma = false;
      for (Entry<?, ?> entry : map.entrySet()) {
         if (addComma) {
            builder.append(',');
         } else {
            addComma = true;
         }
         builder.append('"');
         builder.append(entry.getKey());
         builder.append('"');
         builder.append(':');
         Object value = entry.getValue();
         if (value instanceof String) {
            builder.append('"');
            builder.append(escape((String) value));
            builder.append('"');
         } else if (value instanceof Map<?, ?>) {
            builder.append(toString((Map<?, ?>) value));
         } else if (value instanceof List<?>) {
            builder.append(toString((List<?>) value));
         } else if (value instanceof String[]) {
            builder.append(toString(Arrays.asList(value)));
         } else {
            builder.append(value);
         }
      }
      builder.append('}');
      return builder.toString();
   }

   /**
    * Convert a List instance to JSON
    * 
    * @param list
    *           List to convert
    * @return JSON
    */
   public static String toString(List<?> list) {
      StringBuilder builder = new StringBuilder();
      builder.append('[');
      boolean addComma = false;
      for (Object value : list) {
         if (addComma) {
            builder.append(',');
         } else {
            addComma = true;
         }
         if (value instanceof String) {
            builder.append('"');
            builder.append(escape((String) value));
            builder.append('"');
         } else if (value instanceof Map<?, ?>) {
            builder.append(toString((Map<?, ?>) value));
         } else if (value instanceof List<?>) {
            builder.append(toString((List<?>) value));
         } else if (value instanceof String[]) {
            builder.append(toString(Arrays.asList(value)));
         } else {
            builder.append('"');
            builder.append(value);
            builder.append('"');
         }
      }
      builder.append(']');
      return builder.toString();
   }

   /**
    * Parse provided JSON to a Map
    * 
    * @param json
    *           JSON to parse
    * @return Map instance
    * @throws Exception
    *            if an error occurred
    */
   public static Map<String, Object> toMap(String json) throws Exception {
      return new JsonParser(json).toMap();
   }

   /**
    * Parse provided JSON to a List
    * 
    * @param json
    *           JSON to parse
    * @return List instance
    * @throws Exception
    *            if an error occurred
    */
   public static List<Object> toList(String json) throws Exception {
      return new JsonParser(json).toList();
   }

   private static String escape(String source) {
      char[] escaped = new char[source.length() << 1]; // worst case
      int j = 0;
      for (int i = 0; i < source.length(); i++) {
         char value = source.charAt(i);
         if (value == '\r' || value == '\n' || value == '"') {
            escaped[j++] = '\\';
            if (value == '\r') {
               escaped[j++] = 'r';
            } else if (value == '\n') {
               escaped[j++] = 'n';
            } else if (value == '"') {
               escaped[j++] = '"';
            }
         } else {
            escaped[j++] = value;
         }
      }
      return new String(escaped, 0, j);
   }

}
